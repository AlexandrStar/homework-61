import React from 'react';
import './CountryInfo.css';

const CountryInfo = (props) => {
  return (
    <div className="country">
      {props.oneCountry !== [] ? props.country.map(country => {
          return <div key={country.name}>
                  <h1>{country.name}</h1>
                  <p>Capital: {country.capital}</p>
                  <p>Population: {country.population}</p>
                  <p>Region: {country.region}</p>
                  <p>Subregion: {country.subregion}</p>
                  <img className="flag" src={country.flag} alt="Flag"/>
                  <h3>Borders:</h3>
                  <ul>
                      {country.borders.map(border => {
                        return  <li key={border}>{border}</li>
                      })
                      }
                  </ul>
                </div>
        }
      ): <h3>Выберите страну</h3>}

    </div>
  );
};

export default CountryInfo;