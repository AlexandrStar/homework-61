import React from 'react';
import './List.css';

const List = (props) => {
  return (
    <div className="country-list">
      <ul>
        {props.country.map(country => {
          return <li key={country.name}
                  onClick={(e) => props.onClick(e)}>{country.name}
                </li>
          }
        )}
      </ul>
    </div>
  );
};

export default List;