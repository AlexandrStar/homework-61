import React, { Component } from 'react';
import './App.css';
import axios from 'axios';
import List from "./components/List/List";
import CountryInfo from "./components/CountryInfo/CountryInfo";

class App extends Component {

  state = {
    country: [],
    oneCountry: [],
  };

  componentDidMount() {

    const BASE_URL = 'https://restcountries.eu/';
    const COUNTRY_URL = 'rest/v2/all?fields=name;alpha3Code';

    axios.get(BASE_URL + COUNTRY_URL).then(response => {
      return Promise.all(response.data.map(post => {
        return axios.get(BASE_URL + COUNTRY_URL).then(response => {
          return {...post};
        });
      }));
    }).then(country => {
      this.setState({country});
    }).catch(error => {
      console.log(error);
    });
  };

  showInfoCountry = (e) => {
    const name = e.currentTarget.textContent;
    console.log(name);
    axios.get('https://restcountries.eu/rest/v2/name/' + name).then(response => {
      if (response.data[0].borders.length > 0) {
        Promise.all(response.data[0].borders.map(border => {
          return axios.get(`https://restcountries.eu/rest/v2/alpha/${border}`)
        })).then(result => {
          let borders = [];
          result.map(country => {
            borders.push(country.data.name);
            response.data[0].borders = borders;
            return this.setState({
              oneCountry: response.data
            })
          })
        })
      } else {
        this.setState({oneCountry: response.data});
      }
    })
  };

  render() {
    return (
      <div className="App">
        <List onClick={this.showInfoCountry} country={this.state.country}/>
        <CountryInfo country={this.state.oneCountry}/>
      </div>
    );
  }
}

export default App;
